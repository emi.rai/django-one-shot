from django.shortcuts import render, get_object_or_404, redirect

from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", todo_list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def todo_list_edit(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            lst = form.save()
            return redirect("todo_list_detail", id=lst.id)
    else:
        form = TodoListForm(instance=todo_list)
    context = {
        "todo_list": todo_list,
        "form": form,
    }
    return render(request, 'todos/edit.html', context)


def todo_list_delete(request, id):
    instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        instance.delete()
        return redirect("todo_list_list")
    
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            new_item = form.save()
            return redirect("todo_list_detail", id=new_item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/items/create.html", context)


def todo_item_edit(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            edited = form.save()
            return redirect("todo_list_detail", id=edited.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/items/edit.html", context)
